import "reflect-metadata";
import {
	classToClass,
	Exclude,
	Expose,
	Transform,
	Type,
} from "class-transformer";

class User {
	@Expose()
	id: string;

	@Transform(({ value, obj }) => {
		if (obj.isShowEmail) return value;

		return undefined;
	})
	@Expose()
	email: string;

	@Exclude()
	isShowEmail: boolean;
}

class CustomResponse {
	@Expose()
	@Type(() => User)
	user: User;
}

const user = new User();

user.id = "qwe";
user.isShowEmail = false;
user.email = "qwe@qwe.qwe";

const response = new CustomResponse();

response.user = { ...user };

const transformedReponse = classToClass(response, {
	exposeUnsetFields: false, // remove email if it is undefined
	enableCircularCheck: true, // circular check
	enableImplicitConversion: true, // will transform spread objects too
	excludeExtraneousValues: true, // remove values if it doesn't contains in scheme
});

console.log(transformedReponse);
// CustomResponse { user: User { id: 'qwe' } }